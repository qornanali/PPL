
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script src="http://localhost/PPL/negara/assets/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="http://localhost/PPL/negara/assets/css/bootstrap.min.css">
<script src="http://localhost/PPL/negara/assets/js/jquery.js"></script>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>	
	<div class="container">
	  <div class="row">
		<table border=1>
			<tr>
				<td>Kode</td>
				<td>Nama</td>
				<td>Populasi</td>
				<td>Gambar</td>
			</tr>
			<?php foreach($list_negara as $row=>$value) : ?>
			<tr>
				<td><?= $value->ngr_kd ?></td>
				<td><?= $value->ngr_nama ?></td>
				<td><?= $value->ngr_populasi ?></td>
				<td><img src=<?= "../../assets/image/" . $value->ngr_gambar ?> width="50" height="50"></td>
				<? $base_url = "index.php/c_negara/" ?>
				<td><a href=<?= "detail/" . $value->ngr_kd; ?>>Detail</a></td>
				<td><a href=<?= "update/" . $value->ngr_kd; ?>>Update</a></td>
				<td><a href=<?= "delete/" . $value->ngr_kd; ?>>delete</a></td>
			</tr>
			<?php endforeach; ?>
		</table>
		</div>
	</div>
	
	<nav class="navbar navbar-inverse">
 <div class="container-fluid">
   <div class="navbar-header">
     <a class="navbar-brand" href="index.php?nav=home">Artistant</a>
   </div>
   <ul class="nav navbar-nav navbar-left">
     <li class=<?php echo isset($_GET['nav']) ? ($_GET['nav'] == 'gallery' ?  "active" : "") : (""); ?>>
       <a href="index.php?nav=gallery">Gallery</a>
     </li>
     <form class="navbar-form navbar-left">
       <div class="form-group">
         <input type="text" class="form-control" placeholder="Title or artist's name..">
       </div>
       <button type="submit" class="btn btn-default">Search</button>
     </form>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li class=<?php echo isset($_GET['nav']) ? ($_GET['nav'] == 'blog' ?  "active" : "") : (""); ?>>
        <a href="index.php?nav=blog">Blog</a>
      </li>
      <?php if(!isset($_SESSION['accountname'])){ ?>
    <li class=<?php echo isset($_GET['nav']) ? ($_GET['nav'] == 'login' ?  "active" : "") : (""); ?>>
      <a href="index.php?nav=login">Login</a>
    </li>
    <?php } else {?>
    <li class="dropdown">
       <a class="dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['fullname']; ?>
       <span class="caret"></span></a>
       <ul class="dropdown-menu">
         <li><a href="index.php?nav=profile">My profile</a></li>
         <li><a href="index.php?nav=add_paint">Add new paint</a></li>
         <li><a href="controller/do_logout.php">Logout</a></li>
       </ul>
     </li>
     <?php }?>
     </ul>
 </div>
</nav>

</body>
</html>