<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_negara extends CI_Model {

        public $ngr_kd;
        public $ngr_nama;
        public $ngr_populasi;
        public $ngr_gambar;

        public function get_all(){
        	return $this->db->query("SELECT * FROM t_negara")->result();
        }

        public function get($ngr_kd){
            $this->ngr_kd = $ngr_kd;
        	return $this->db->query("SELECT * FROM t_negara WHERE ngr_kd = " . $this->ngr_kd)->result();
        }

        public function insert(){
        	$this->ngr_kd = $this->input->post('ngr_kd');
            $this->ngr_nama = $this->input->post('ngr_nama');
            $this->ngr_populasi = $this->input->post('ngr_populasi');
            $this->ngr_gambar = $this->input->post('ngr_gambar');

        	$this->db->query("INSERT INTO t_negara(ngr_kd, ngr_nama, ngr_populasi, ngr_gambar) 
        		VALUES (" . $this->ngr_kd . ", '". $this->ngr_nama ."', " . $this->ngr_populasi . ", 
                '". $this->ngr_gambar ."');");
        }

        public function delete($ngr_kd){
            $this->ngr_kd = $ngr_kd;
        	$this->db->query("DELETE FROM t_negara WHERE ngr_kd = " . $this->ngr_kd . ";");
        }

        public function update($ngr_kd){
        	$this->ngr_kd = $ngr_kd;
            $this->ngr_nama = $this->input->post('ngr_nama');
            $this->ngr_populasi = $this->input->post('ngr_populasi');
            $this->ngr_gambar = $this->input->post('ngr_gambar');

        	$this->db->query("UPDATE t_negara SET 
                ngr_nama = '". $this->ngr_nama ."', 
                ngr_populasi = ". $this->ngr_populasi .", 
                ngr_gambar = '". $this->ngr_gambar ."' 
                WHERE ngr_kd = " . $this->ngr_kd . ";");
        }
}
