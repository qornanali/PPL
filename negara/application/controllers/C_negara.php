<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_negara extends CI_Controller {

	public function index(){
		$this->view();
	}

	function __construct(){
		parent::__construct();
		$this->load->model('m_negara');
	}

	public function create(){
		if($this->input->post('submit')){
			$this->m_mahasiswa->insert();
			redirect('c_negara/view');
		}
		// $this->load->view('v_add_negara');
	}

	public function update(){
		$ngr_kd = $this->uri->segment(3);
		$data['negara'] = $this->m_negara->get($ngr_kd);
		if($this->input->post('submit')){
			$this->m_negara->update($ngr_kd);
			redirect('m_negara/view');
		}
		// $this->load->view('v_add_negara', $data);
	}

	public function view(){
		$data['title'] = "List Negara";
		$data['list_negara'] = $this->m_negara->get_all();
		$this->load->view('v_list_negara', $data);
	}

	public function detail(){
		$ngr_kd = $this->uri->segment(3);
		$data['negara'] = $this->m_negara->get($ngr_kd);
		// $this->load->view('v_list_negara', $data);
	}

	public function delete(){
		$ngr_kd = $this->uri->segment(3);
		$this->m_negara->delete($ngr_kd);
		$this->view();
	}
}
