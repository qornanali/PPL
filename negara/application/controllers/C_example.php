<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_example extends CI_Controller {

	public function index(){
		$this->view();
	}

	function __construct(){
		parent::__construct();
	}

	public function view(){
		$this->load->view('v_example');
	}
}
