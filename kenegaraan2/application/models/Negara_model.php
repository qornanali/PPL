<?php 
/**
* 
*/
class Negara_model extends CI_Model
{
	
	function get()
	{
		return $this->db->get('t_negara')->result();
	}
	function getById($kd)
	{
		return $this->db->get_where('t_negara',array('ngr_kd' => $kd))->row();
	}
	function insert($data){
		$this->db->insert('t_negara',$data);
	}

	function update($kd, $data){
		$this->db->where('ngr_kd',$kd)
				->update('t_negara',$data);
	}
}