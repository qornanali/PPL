<?php 
/**
* 
*/
class Auth extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_model');
	}
	function index(){
		$data['title'] = 'Login';
		if($this->session->userdata('login'))
			redirect('home');

		$this->load->view('auth/login',$data);
	}
	function login(){
		if($this->input->post()){
			$data = array(
				'adm_nip' => $this->input->post('adm_nip'),
				'adm_nick' => $this->input->post('adm_nick'),
				'adm_pw' => md5($this->input->post('adm_pw')),
			);
			$login = $this->Auth_model->login($data);
			if($login->num_rows() > 0){
				$user = $login->row();
				$session = array(
					'login' => true,
					'adm_nick' => $user->adm_nick,
					'adm_nip' => $user->adm_nip,
					'adm_nama' => $user->adm_nama,
				);
				$this->session->set_userdata($session);
			}
			else{
				$this->session->set_flashdata('alert','login gagal');
			}
		}
		redirect('auth');
	}
	function logout(){
		$this->session->sess_destroy();
		redirect('auth');
	}
}