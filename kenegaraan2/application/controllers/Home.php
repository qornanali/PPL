<?php 
/**
* 
*/
class Home extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('login')){
			redirect('auth');
		}
	}
	function index(){
		$data['title'] = 'Home';
		$this->template->view('home/index',$data);
	}
}