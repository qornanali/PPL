<?php 
/**
* 
*/
class Negara extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		// if(!$this->session->userdata('login')){
		// 	redirect('auth');
		// }
		$this->load->model('Negara_model');
	}
	function index(){
		$data['title'] = 'Negara';
		$data['result'] = $this->Negara_model->get();
		$this->template->view('negara/index',$data);
	}
	function tambah(){
		if($this->input->post()){
			$config['upload_path'] = './assets/images/';
    	    $config['allowed_types'] = '*';
                
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('ngr_gambar')){
				$this->session->set_flashdata('alert',$this->upload->display_errors());
            }
            else{

				$data = array(
					'ngr_nama' => $this->input->post('ngr_nama'),
					'ngr_populasi' => $this->input->post('ngr_populasi'),
					'ngr_gambar' => $this->upload->file_name,
				);

				$this->Negara_model->insert($data);

				$this->session->set_flashdata('alert','berhasil ditambahkan!');
			}
			redirect('negara');
		}
		else{
			$data['title'] = 'Tambah negara';
			$this->template->view('negara/tambah',$data);
		}
	}
	function update($id){
		if($this->input->post()){
			$config['upload_path'] = './assets/images/';
    	    $config['allowed_types'] = '*';
                
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('ngr_gambar')){
				$data = array(
					'ngr_nama' => $this->input->post('ngr_nama'),
					'ngr_populasi' => $this->input->post('ngr_populasi'),
				);
            }
            else{
				$data = array(
					'ngr_nama' => $this->input->post('ngr_nama'),
					'jml_penduduk' => $this->input->post('jml_penduduk'),
					'ngr_gambar' => $this->upload->file_name,
				);
			}
			$negara = $this->Negara_model->getById($id);
			
			unlink('assets/images/'.$negara->ngr_gambar);

			$this->Negara_model->update($id,$data);
			$this->session->set_flashdata('alert','berhasil diupdate!');

			redirect('negara');
		}
		else{
			$data['title'] = 'Update negara';
			$data['result'] = $this->Negara_model->getById($id);
			$this->template->view('negara/update',$data);
		}
	}
}