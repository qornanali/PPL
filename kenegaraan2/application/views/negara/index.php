<div style="margin-left: auto; margin-right: auto;">
	<h3 style="text-align: center;">List Kota</h3>
	<!-- <div style="text-align: right; margin-right: 400px;">
		<a href="<?php echo site_url('kota/tambah') ?>">Tambah Kota</a>
	</div> -->

	<form method="post" action="<?php echo site_url('negara/tambah') ?>" enctype="multipart/form-data" style="margin-left: auto; margin-right: auto;">
		<div style="width:250px; margin-left: auto; margin-right: auto;">
			<label>NAMA</label> <br>
			<input type="text" name="ngr_nama"> <br>

			<label>POPULASI</label> <br>
			<input type="number" name="ngr_populasi"> <br>

			<label>FOTO</label> <br>
			<input type="file" name="ngr_gambar"> <br>
			<br>
			<input type="submit" name="submit" value="Submit">
		</div>
	</form>
	<br>
	<h4 style="text-align: center;"><?php echo $this->session->flashdata('alert') ?></h4>
	<br>
	<table border="1" style="margin-left: auto; margin-right: auto;">
		<tr>
			<th>KODE</th>
			<th>NAMA</th>
			<th>POPULASI</th>
			<th>FOTO</th>
		</tr>
		<?php foreach($result as $row): ?>
			<tr>
				<td width="40px"><?php echo $row->ngr_kd ?></td>
				<td><?php echo $row->ngr_nama ?></td>
				<td><?php echo $row->ngr_populasi ?></td>
				<td width="200px">
					<img src="<?php echo base_url('assets/images/'.$row->ngr_gambar) ?>"  width="200px">
				</td>
				<td>
					<a href="<?php echo site_url('negara/update/'.$row->ngr_kd) ?>">Update</a>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
</div>