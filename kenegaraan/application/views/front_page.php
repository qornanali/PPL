<!DOCTYPE html>
<html lang="en">
<head>
  <title>Admin Negara</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">   
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
      color: blue;
    }
    header {
      margin-bottom: 0;
      border-radius: 0;
      color: blue;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: white;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      /*background-color: #666;
      color: white;*/
      margin-bottom: 0;
      border-radius: 0;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>
<!-- <?php $this->load->view('menu.php'); ?> -->
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav" style="height: 100%">
      <p><a href="#"></a></p>
      <p><a href="#"></a></p>
      <p><a href="#"></a></p>
    </div>
    <div class="col-sm-8 text-left"> 
    <?php $this->load->view($page); ?>
    </div>
   <!--  <div class="col-sm-2 sidenav">
      <div class="well">
        <p>ADS</p>
      </div>
      <div class="well">
        <p>ADS</p>
      </div>
    </div> -->
  </div>
</div>

<?php $this->load->view('footer.php'); ?>
</body>
</html>
