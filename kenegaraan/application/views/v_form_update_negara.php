<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Form Mahasiswa</title>
</head>
<body>
   <form method="POST" action="<?=base_url('index.php/c_negara/update')?>">
      <table>
         <input type="hidden" name="ngr_kd" value="<?=$negara->ngr_kd?>">
         <tr>
            <td>ID</td>
            <td>:</td>
            <td><input type="number" name="ngr_kd" value="<?=$negara->ngr_kd?>" disabled></td>
         </tr>
           <tr>
            <td>Nama Negara</td>
            <td>:</td>
            <td><input type="text" name="ngr_nama" value="<?=$negara->ngr_nama?>"></td>
         </tr>
           <tr>
            <td>Jumlah Penduduk</td>
            <td>:</td>
            <td><input type="number" name="ngr_populasi" value="<?=$negara->ngr_populasi?>"></td>
         </tr> 
         <tr>
            <td>Gambar</td>
            <td>:</td>
            <td><input type="file" name="ngr_gambar"></td>
         </tr>
         <tr>
            <td align="right"> <button type="submit">Submit</button></td>
         </tr>
      </table>
   </form>
   
</body>
</html>