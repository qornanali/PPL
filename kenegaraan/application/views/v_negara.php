<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

   <div width="100%" style="margin-top: 60px;">
         <div class="row" style="padding-bottom: 10px;">
            <div class="col-lg-3">
                 <a href="<?=base_url('index.php/c_negara/tambah')?>">
                  <button class="btn btn-success">Tambah Data</button>
                  </a>
            </div>
<!--             <div class="col-lg-9">
               <form method="POST" action="<?=base_url('index.php/c_negara/display')?>" align="right">
                  <input type="text" name="search" placeholder="Cari Negara">
                  <button class="btn">Cari</button>
               </form>
            </div> -->
            </div>

        
      <table border="1" align="center" class="table table-hover">
          <thead>
            <tr>
              <th class="text-center">ID</th>
              <th class="text-center">Nama</th>
              <th class="text-center">Jumlah Penduduk</th>
              <th class="text-center">Gambar</th>
              <th></th>
           </tr>
        </thead>
        <tbody>
         <?php if($negara != null): ?>
            <?php foreach($negara as $key => $value):?> 
         	  <tr>
         	  	<td><?=$value->ngr_kd?></td>
         	  	<td><?=$value->ngr_nama?></td>
         	  	<td><?=$value->ngr_populasi?></td>
              <td><img width="100" height="100" src="<?= base_url('assets/images/').$value->ngr_gambar ?>"</td>
               <td class="text-center">
                  <a href="<?=base_url('index.php/c_negara/ubah/').$value->ngr_kd;?>"><button class="btn btn-warning">Update</button></a>
                  <a href="<?=base_url('index.php/c_negara/delete/').$value->ngr_kd;?>"><button class="btn btn-danger">Delete</button></a>
                  <a href="<?=base_url('index.php/c_negara/detail/').$value->ngr_kd;?>"><button class="btn btn-info">View</button></a>
         	  </tr>
            <?php endforeach; ?>
         <?php endif;?>
         <?php if($negara == null): ?>
            <tr colspan="4">
               <td colspan="4" class="text-center">Pencarian Tidak Ditemukan</td>
            </tr>
         <?php endif;?>
         </tbody>
      </table>
   </div>
