<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_negara extends CI_Model {

  private $table_name = "t_negara";
  private $table_name_id = "ngr_kd";
  
	function __construct() 
	{
         parent::__construct();
    }


	public function get_all($query=null)
	{
		$data=null;
		$this->db->select('*');
		$this->db->from($this->table_name);
		if($query != null){
			$this->db->like("ngr_nama",$query);
		}
		$query=$this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	function insert($data){
	    $this->db->trans_start();
        $this->db->insert($this->table_name,$data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return true;
		}

	}

	function delete($id) {
		$this->db->trans_start();
		$this->db->where($this->table_name_id, $id);
		$this->db->delete($this->table_name);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return true;
		}
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}
	
	function get_by_id($id) {
		$this->db->where($this->table_name_id, $id);
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	function update($data, $id) {
		$this->db->trans_start();
		$this->db->update($this->table_name,$data,array($this->table_name_id=>$id));
        $this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			$data['status'] = false;
			$data['error']=$this->db->_error_message();
			return $data;
		}else{
			$this->db->trans_commit();
			$data['status'] = true;
			return $data;
		}
	}

}
?>