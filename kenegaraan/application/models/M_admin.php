<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_admin extends CI_Model {

  private $table_name = "t_admin";
  private $table_name_id = "adm_nip";
  
	function __construct() 
	{
         parent::__construct();
    }

    public function login($id,$username,$password){
    	$this->db->where($this->table_name_id, $id, $username, $password);
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;

    }
}