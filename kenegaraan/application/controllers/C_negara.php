<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_negara extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	function __construct() {
	    parent::__construct();
	    $this->load->model('m_negara', '', TRUE);
  	}


	public function index() 
	{
		$data['page']="home";
		$this->load->view('front_page',$data);
		// $this->display();
	}

	public function display()
	{
		if($this->input->post('search')){
			$data['negara'] = $this->m_negara->get_all($this->input->post('search'));
		}else{
			$data['negara'] = $this->m_negara->get_all();
		}

		$data['page']="v_negara";
		$this->load->view('front_page',$data);
	}

	public function tambah()
	{
		$data['page']="v_form_negara";
		$this->load->view('front_page',$data);
	}

	public function ubah($id)
	{
		$data['negara'] = $this->m_negara->get_by_id($id); 
		$data['page']="v_form_update_negara";
		$this->load->view('front_page',$data);
	}

	public function insert()
	{
		$ngr_nama = $this->input->post('ngr_nama');
		$ngr_populasi = $this->input->post('ngr_populasi');

		$data = array('ngr_nama'=>$ngr_nama,
					  'ngr_populasi'=>$ngr_populasi
					 );

		$this->m_negara->insert($data);
		redirect('c_negara/display');

	}

	public function delete($id)
	{
		if($this->m_negara->delete($id))
		{
			redirect('c_negara/display');
		}
	}

	public function detail($id)
	{
		$data['negara'] = $this->m_negara->get_by_id($id);
		$data['page']="v_detail_negara";
		$this->load->view('front_page',$data);
	}

	public function update() 
	{	
		$ngr_kd=   $this->input->post('ngr_kd');
		$ngr_nama = $this->input->post('ngr_nama');
		$ngr_populasi = $this->input->post('ngr_populasi');

		$data = array('ngr_kd'=>$ngr_kd,
					  'ngr_nama'=>$ngr_nama,
					  'ngr_populasi'=>$ngr_populasi
					 );

		$this->m_negara->update($data,$ngr_kd);
		redirect('c_negara/display');
	}
}
