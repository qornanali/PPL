<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_mahasiswa extends CI_Model {

        public $nim;
        public $nama;
        public $umur;

        public function get_all(){
        	return $this->db->query("SELECT * FROM mahasiswa")->result();
        }

        public function get($nim){
            $this->nim = $nim;
        	return $this->db->query("SELECT * FROM mahasiswa WHERE nim = " . $this->nim)->result();
        }

        public function insert(){
        	$this->nim = $this->input->post('nim');
            $this->nama = $this->input->post('nama');
            $this->umur    = $this->input->post('umur');

        	$this->db->query("INSERT INTO mahasiswa(nim, nama, umur) 
        		VALUES (" . $this->nim . ", '". $this->nama ."', " . $this->umur . ");");
        }

        public function delete($nim){
            $this->nim = $nim;
        	$this->db->query("DELETE FROM mahasiswa WHERE nim = " . $this->nim . ";");
        }

        public function update($nim){
        	$this->nim = $nim;
            $this->nama = $this->input->post('nama');
            $this->umur = $this->input->post('umur');

        	$this->db->query("UPDATE mahasiswa SET nama = '". $this->nama ."', umur = '". $this->umur ."' WHERE nim = " . $this->nim . ";");
        }
}
