<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_mahasiswa extends CI_Controller {

	public function index(){
		$this->view();
	}

	function __construct(){
		parent::__construct();
		$this->load->model('m_mahasiswa');
	}

	public function create(){
		if($this->input->post('submit')){
			$this->m_mahasiswa->insert();
			redirect('c_mahasiswa/view');
		}
		$this->load->view('v_add_mahasiswa');
	}

	public function update(){
		$nim = $this->uri->segment(3);
		$data['mahasiswa'] = $this->m_mahasiswa->get($nim);
		if($this->input->post('submit')){
			$this->m_mahasiswa->update($nim);
			redirect('c_mahasiswa/view');
		}
		$this->load->view('v_add_mahasiswa', $data);
	}

	public function view(){
		$data['list_mahasiswa'] = $this->m_mahasiswa->get_all();
		$this->load->view('v_display_list_mahasiswa', $data);
	}

	public function detail(){
		$nim = $this->uri->segment(3);
		$data['mahasiswa'] = $this->m_mahasiswa->get($nim);
		$this->load->view('v_display_mahasiswa', $data);
	}

	public function delete(){
		$nim = $this->uri->segment(3);
		$this->m_mahasiswa->delete($nim);
		$this->view();
	}
}
