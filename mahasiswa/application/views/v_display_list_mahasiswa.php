<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Data Mahasiswa</title>
</head>
<body>
	<a href="create">Tambah Mahasiswa</a><br>
	<table border=1>
		<tr>
			<td>NIM</td>
			<td>Nama</td>
			<td>Umur</td>
			<td>Foto</td>
		</tr>
		<?php foreach($list_mahasiswa as $row=>$value) : ?>
		<tr>
			<td><?= $value->nim ?></td>
			<td><?= $value->nama ?></td>
			<td><?= $value->umur ?></td>
			<td><img src=<?= "../../assets/" . $value->foto ?> width="50" height="50"></td>
			<? $base_url = "index.php/c_mahasiswa/" ?>
			<td><a href=<?= "detail/" . $value->nim; ?>>Detail</a></td>
			<td><a href=<?= "update/" . $value->nim; ?>>Update</a></td>
			<td><a href=<?= "delete/" . $value->nim; ?>>delete</a></td>
		</tr>
		<?php endforeach; ?>
	</table>
	</div>
</body>
</html>