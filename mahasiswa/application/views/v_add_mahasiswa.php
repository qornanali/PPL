<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Tambah Mahasiswa</title>
	</head>
	<body>
		<h3>Tambah Data</h3>
		<? if(isset($mahasiswa)) { foreach($mahasiswa as $row=>$value) : endforeach; }
		echo form_open_multipart(isset($mahasiswa) ? 'c_mahasiswa/update/' . $value->nim : 'c_mahasiswa/create'); ?>
		<table>
			<tr>
				<td>Nim</td>
				<td>:</td>
				<td>
				<? 
				$data = array(
					'name'          => 'nim',
					'type'			=> 'text',
					'value'			=> isset($mahasiswa) ? $value->nim : ""
					);
				if(isset($mahasiswa)){
					$data['disabled'] = "true";
				}
				echo form_input($data); ?>
				</td>	
			</tr>

			<tr>
				<td>Nama</td>
				<td>:</td>
				<td>
				<? echo form_input(array(
					'name'          => 'nama',
					'type'			=> 'text',
					'value'			=> isset($mahasiswa) ? $value->nama : ""
					)); ?>						
				</td>
			</tr>

			<tr>
				<td>Umur</td>
				<td>:</td>
				<td>
				<? echo form_input(array(
					'name'          => 'umur',
					'type'			=> 'text',
					'value'			=> isset($mahasiswa) ? $value->umur : ""
					)); ?>
				</td>	
			</tr>

			<tr>
				<td>Foto</td>
				<td>:</td>
				<td>
				<? echo form_input(array(
					'name'          => 'foto',
					'type'			=> 'file',
					'size'			=> '20'
					)); ?>
				</td>	
			</tr>
		</table>
		<table>
			<tr>
				<td>
				<? echo form_submit('submit', isset($mahasiswa) ? "Ubah" : "Simpan"); ?>
				</td>
				<? echo form_close(); ?>
			</tr>
		</table>
		<a href=<?= isset($mahasiswa) ? "../view" : "view" ?>>Kembali</a><br>
	</body>
</html>